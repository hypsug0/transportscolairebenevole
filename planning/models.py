import datetime
import uuid

from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class BaseModel(models.Model):
    """Generic base model with standard metadatas"""

    timestamp_create = models.DateTimeField(auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(
        User,
        null=True,
        blank=True,
        db_index=True,
        editable=False,
        related_name="+",
        on_delete=models.SET_NULL,
    )
    updated_by = models.ForeignKey(
        User,
        null=True,
        blank=True,
        db_index=True,
        editable=False,
        related_name="+",
        on_delete=models.SET_NULL,
    )

    class Meta:
        abstract = True


class Kid(BaseModel):
    name = models.CharField(
        max_length=100, blank=False, null=False, verbose_name=_("name")
    )
    referent = models.ManyToManyField(
        User, verbose_name=_("fathers"), related_name="kids"
    )

    def __str__(self):
        return self.name


# Create your models here.
class Tour(BaseModel):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    start = models.DateTimeField(
        blank=False, null=False, verbose_name=_("start")
    )
    end = models.DateTimeField(blank=True, null=True, verbose_name=_("end"))
    companion = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name="tours",
        on_delete=models.SET_NULL,
        verbose_name=_("companion"),
        name="companion",
    )
    kids = models.ManyToManyField(Kid, verbose_name=_("kid"), blank=True)

    @property
    def kids_count(self):
        """Returns kids count for each tour

        Returns:
            int: Number of kids
        """
        return self.kids.count()

    @property
    def title(self):
        return _(f"{self.companion or '!!!'} {str(self.kids_count)} kids")

    @property
    def day_time(self):
        """Returns kids count for each tour

        Returns:
            int: Number of kids
        """
        return (
            _("morning")
            if self.start.time() < datetime.time(12)
            else _("afternoon")
        )

    def __str__(self):
        return str(self.start.date())

    def get_absolute_url(self):
        return reverse("tour_detail", kwargs={"pk": self.pk})
