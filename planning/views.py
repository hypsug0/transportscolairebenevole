import datetime
from typing import Any

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import EmailMessage
from django.db.models.query import QuerySet
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView, UpdateView
from django_filters.views import FilterView

from .filters import TourFilter
from .forms import TourForm
from .models import Kid, Tour
from .pdf import pdf_week_planning
from .utils import week_magic

User = get_user_model()


def default_time_filter() -> datetime:
    """Get default time filter (morning or afternoon tours)

    Returns:
        datetime: datetime filter
    """
    now = datetime.datetime.now()
    hour = 0 if now.hour < 12 else 12
    result = now.replace(hour = hour, minute=0, second=0, microsecond=0)
    return result


def datetime_interval(months: int) -> datetime:
    """calculate future date from month interval

    Args:
        months (int): Number of months

    Returns:
        datetime: Future date (in n months)
    """
    today = datetime.datetime.now()
    delta = datetime.timedelta(days=months * 365 / 12)
    return today + delta


class GenericTourListView(LoginRequiredMixin, FilterView):
    model = Tour
    template_name = "tour_list.html"
    queryset = (
        Tour.objects.order_by("start")
        .select_related("companion")
        .prefetch_related("kids")
    )
    filterset_class = TourFilter

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["kids"] = Kid.objects.all()
        return context


class HomeView(GenericTourListView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("Next tours")
        context["home"] = True
        return context

    def get_queryset(self) -> QuerySet[Any]:
        qs = super().get_queryset()
        date = default_time_filter()
        return qs.filter(start__gte=date).filter(
            start__lte=datetime_interval(3)
        )


class TourListView(GenericTourListView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("All tours")
        context["kids"] = Kid.objects.all()

        # context["now"] = datetime.datetime.now()
        return context


class TourDetailView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Tour
    template_name = "tour_detail.html"
    # fields = ["companion", "kids"]
    form_class = TourForm
    success_url = reverse_lazy("home")
    success_message = _("Your update have been successfully saved")
    queryset = Tour.objects.select_related("companion").prefetch_related(
        "kids"
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["companions"] = User.objects.all()
        context["kids"] = Kid.objects.all()
        # context["self_kids"] = serializers.serialize(
        #     "json",
        #     Kid.objects.filter(referent=user).all()
        #     if not (user.is_superuser or user.is_staff)
        #     else Kid.objects.all(),
        # )

        return context


class CalendarView(LoginRequiredMixin, TemplateView):
    template_name = "calendar.html"


def week_planning(request, moment: str = None):
    print(moment)
    if moment and moment == "next":
        d = datetime.datetime.today() + datetime.timedelta(days=7)
    else:
        d = datetime.datetime.today()
    response = HttpResponse(content_type="application/pdf")
    file_date = datetime.datetime.today().strftime("%Y-%m-%d")
    response["Content-Disposition"] = f"inline; filename={file_date}.pdf"
    date_min, date_max = week_magic(d.date())
    data = Tour.objects.filter(
        start__gte=date_min, start__lte=date_max
    ).order_by("start")
    pdf = pdf_week_planning(request, data, date_min, date_max)
    response.write(pdf)
    return response


def email_week_planning_pdf(request, moment: str = None):
    if moment == "next":
        d = datetime.datetime.today() + datetime.timedelta(days=7)
    else:
        d = datetime.datetime.today()
    date_min, date_max = week_magic(d.date())
    data = Tour.objects.filter(
        start__gte=date_min, start__lte=date_max
    ).order_by("start")
    pdf = pdf_week_planning(request, data, date_min, date_max)
    subject = f"{settings.EMAIL_SUBJECT_PREFIX} Planning de la semaine {'prochaine' if moment == 'next' else '' }"
    message = f"""Bonjour,

Veuillez trouver en PJ le planning de bus scolaire pour {'la semaine à venir' if moment == 'next' else 'cette semaine' }

Bien cordialement,

Toute l'équipe
{settings.SITE_NAME}
"""
    mail = EmailMessage(
        subject, message, settings.EMAIL_HOST_USER, settings.PDF_EMAILS
    )
    mail.attach("bus_planning.pdf", pdf, "application/pdf")

    try:
        mail.send(fail_silently=False)
        return HttpResponse("Mail Sent")
    except Exception as e:
        return HttpResponse(f"Mail Not Sent : {str(e)}")
