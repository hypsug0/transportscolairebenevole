from django import forms

from .models import Tour


class TourForm(forms.ModelForm):
    class Meta:
        model = Tour
        fields = ["companion", "kids"]
        widgets = {
            "kids": forms.CheckboxSelectMultiple,
        }

    # def __init__(self, *args, **kwargs):
    #     self.user = kwargs.pop(
    #         "user"
    #     )  # To get request.user. Do not use kwargs.pop('user', None) due to potential security hole

    #     super().__init__(*args, **kwargs)
    #     self.fields["kids"].queryset = Kid.objects.filter(referent=self.user).all()


# class SubscribeKidsForm(forms.Form):
#     tours = forms.MultipleChoiceField(
#         widget=forms.CheckboxSelectMultiple, choices=Tour.objects.all()
#     )
#     kids = forms.MultipleChoiceField(
#         widget=forms.CheckboxSelectMultiple, choices=Kid.objects.all()
#     )
