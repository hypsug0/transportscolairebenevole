import django_filters
from django import forms

from .models import Kid, Tour


class TourFilter(django_filters.FilterSet):
    kids = django_filters.ModelMultipleChoiceFilter(
        queryset=Kid.objects.all(), widget=forms.CheckboxSelectMultiple()
    )

    class Meta:
        model = Tour
        fields = ["companion", "kids"]
