document.addEventListener("DOMContentLoaded", () => {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(
    document.querySelectorAll(".navbar-burger"),
    0
  );

  // Add a click event on each of them
  $navbarBurgers.forEach((el) => {
    el.addEventListener("click", () => {
      // Get the target from the "data-target" attribute
      const target = el.dataset.target;
      const $target = document.getElementById(target);

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      el.classList.toggle("is-active");
      $target.classList.toggle("is-active");
    });
  });
});

// To access to bulmaCalendar instance of an element
const element = document.querySelector("#my-element");
if (element) {
  // bulmaCalendar instance is available as element.bulmaCalendar
  element.bulmaCalendar.on("select", (datepicker) => {
    console.log(datepicker.data.value());
  });
}

const loadCal = () => {
  console.log("window", window);
  const container = document.getElementById("calendar");
  const Calendar = window.tui.Calendar;

  console.log(Calendar, tui);
  const options = {
    usageStatistics: false,
    defaultView: "week",
    timezone: {
      zones: [
        {
          timezoneName: "Asia/Seoul",
          displayLabel: "Seoul",
        },
        {
          timezoneName: "Europe/London",
          displayLabel: "London",
        },
      ],
    },
    calendars: [
      {
        id: "cal1",
        name: "Personal",
        backgroundColor: "#03bd9e",
      },
      {
        id: "cal2",
        name: "Work",
        backgroundColor: "#00a9ff",
      },
    ],
  };

  const calendar = new Calendar(container, options);
  calendar.createEvents([
    {
      id: "event1",
      calendarId: "cal2",
      title: "Weekly meeting",
      start: "2022-06-07T09:00:00",
      end: "2022-06-07T10:00:00",
    },
    {
      id: "event2",
      calendarId: "cal1",
      title: "Lunch appointment",
      start: "2022-06-08T12:00:00",
      end: "2022-06-08T13:00:00",
    },
    {
      id: "event3",
      calendarId: "cal2",
      title: "Vacation",
      start: "2022-06-08",
      end: "2022-06-10",
      isAllday: true,
      category: "allday",
    },
  ]);
};

// const elementsInputs = document.querySelectorAll('input[id^="id_"]');
// elementsInputs.forEach((element) => {
//   console.log("input", element);
//   element.classList.add("input");
// });
// const elementsLabels = document.querySelectorAll('label[for^="id_"]');
// elementsLabels.forEach((element) => {
//   console.log("input", element);
//   element.classList.add("label");
// });

document.addEventListener("DOMContentLoaded", () => {
  // Functions to open and close a modal
  function openModal($el) {
    $el.classList.add("is-active");
  }

  function closeModal($el) {
    $el.classList.remove("is-active");
  }

  function closeAllModals() {
    (document.querySelectorAll(".modal") || []).forEach(($modal) => {
      closeModal($modal);
    });
  }

  // Add a click event on buttons to open a specific modal
  (document.querySelectorAll(".js-modal-trigger") || []).forEach(($trigger) => {
    const modal = $trigger.dataset.target;
    const $target = document.getElementById(modal);

    $trigger.addEventListener("click", () => {
      openModal($target);
    });
  });

  // Add a click event on various child elements to close the parent modal
  (
    document.querySelectorAll(
      ".modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button"
    ) || []
  ).forEach(($close) => {
    const $target = $close.closest(".modal");

    $close.addEventListener("click", () => {
      closeModal($target);
    });
  });

  // Add a keyboard event to close all modals
  document.addEventListener("keydown", (event) => {
    if (event.code === "Escape") {
      closeAllModals();
    }
  });
});

document.addEventListener("DOMContentLoaded", () => {
  (document.querySelectorAll(".notification .delete") || []).forEach(
    ($delete) => {
      const $notification = $delete.parentNode;
      console.log("DELETE", $delete, $notification);
      $delete.addEventListener("click", () => {
        $notification.parentNode.removeChild($notification);
      });
    }
  );
});
