# Generated by Django 4.2.9 on 2024-01-31 19:36

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("planning", "0003_tour_uuid"),
    ]

    operations = [
        migrations.AlterField(
            model_name="kid",
            name="referent",
            field=models.ManyToManyField(
                related_name="kids",
                to=settings.AUTH_USER_MODEL,
                verbose_name="fathers",
            ),
        ),
    ]
