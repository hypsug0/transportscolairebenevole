import datetime
from io import BytesIO

from django.conf import settings
from django.utils.translation import gettext_lazy as _
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas


def daytime(date):
    hour = date.hour
    return _("morning") if hour < 12 else _("afternoon")


def pdf_week_planning(request, data, date_min, date_max):
    print(f"{request}, {data}")
    buffer = BytesIO()
    c = canvas.Canvas(buffer, pagesize=A4)
    # Start writing
    c.setFont("Helvetica-Bold", 16, leading=None)
    x1 = 40
    y1 = 750
    c.drawString(
        40,
        y1,
        f"{settings.SITE_NAME}",
    )
    y1 = y1 - 20
    c.drawString(
        40,
        y1,
        f"Planning de la semaine du {date_min.strftime('%d/%m/%Y')} au {date_max.strftime('%d/%m/%Y')}",
    )
    y1 = y1 - 15
    c.setFont("Helvetica", 10, leading=None)
    c.drawString(
        40,
        y1,
        f"Automatiquement généré le {datetime.datetime.now().strftime('%d/%m/%Y %H:%M')}",
    )

    y1 = y1 - 50
    for d in data:
        companion = (
            d.companion.get_full_name()
            if d.companion
            else "Pas d'accompagnateur"
        )
        c.setFont("Helvetica-Bold", 15, leading=None)
        c.drawString(
            x1,
            y1 - 12,
            f"{d.start.strftime('%d/%m/%Y')} {daytime(d.start)} : {companion}",
        )
        c.setFont("Helvetica", 15, leading=None)
        kids = ", ".join([k.name for k in d.kids.all()])
        c.drawString(
            x1,
            y1 - 30,
            f"Enfants: {kids if d.kids.count() > 0 else 'Aucun enfant'}"
            f"{' (Pas de prise en charge)' if not d.companion else ''}",
        )
        y1 = y1 - 60

    # End writing
    c.showPage()
    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    return pdf
