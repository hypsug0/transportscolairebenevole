from django.urls import path

from .feeds import TourFeed
from .views import (
    HomeView,
    TourDetailView,
    TourListView,
    email_week_planning_pdf,
    week_planning,
)

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("tours", TourListView.as_view(), name="tours"),
    path("tours/<int:pk>", TourDetailView.as_view(), name="tour_detail"),
    path("cal/feed.ics", TourFeed(), name="feed"),
    path("cal/<int:pk>/feed.ics", TourFeed(), name="personnal_feed"),
    path("email_pdf", email_week_planning_pdf, name="send_mail"),
    path(
        "email_pdf/<str:moment>",
        email_week_planning_pdf,
        name="send_mail_moment",
    ),
    path("pdf/<str:moment>", week_planning, name="week_planning_moment"),
    path("pdf", week_planning, name="week_planning"),
]
