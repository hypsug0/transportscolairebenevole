from django.contrib import admin

from .models import Kid, Tour

# Register your models here.


class KidAdmin(admin.ModelAdmin):
    list_display = ["name", "get_referent"]

    def get_referent(self, obj):
        return "\n".join([p.username for p in obj.referent.all()])


class TourAdmin(admin.ModelAdmin):
    list_display = ["start", "companion", "get_kids"]

    def get_kids(self, obj):
        return "\n".join([p.name for p in obj.kids.all()])


admin.site.register(Tour, TourAdmin)
admin.site.register(Kid, KidAdmin)
