import datetime

from django.utils.translation import gettext_lazy as _
from django_ical.views import ICalFeed

from .models import Tour


class TourFeed(ICalFeed):
    """
    A simple event calender
    """

    product_id = "-//bus.scolaire.targezed.net//tours//FR"
    timezone = "UTC+2"
    file_name = "feed.ics"

    def items(self, tour):
        print(f'tour {tour} {dir(tour)}')
        return Tour.objects.filter(
            start__gte=datetime.datetime.now()
        ).order_by("start")

    def item_title(self, item):
        companion = (
            item.companion.get_full_name()
            if item.companion
            else _("Undefined")
        )
        return _(f"{companion} ({item.kids.count()} subscribed kids)")

    def item_description(self, item):
        kids = ", ".join([k.name for k in item.kids.all()])
        return _(f"Subscribed kids : {kids}" if kids else "No kids")

    def item_start_datetime(self, item):
        return item.start

    def item_end_datetime(self, item):
        return item.end

    def item_guid(self, item):
        return f"{item.uuid}@transport.scolaire"

    def item_link(self, _item):
        return "/"
