/* Création de nouveaux enfants 
 * Leur rattachement aux parents se fait dans le backoffice */
BEGIN;

INSERT INTO transportscolaire.planning_kid (name , timestamp_create , timestamp_update)
SELECT
    name
    , now()
    , now()
FROM (
    SELECT
        unnest(ARRAY['Aaron']) AS name) AS t;

COMMIT;


/* Création des tournées de l'année (avec un accompagnateur 
 * récurrent tous les mardi et vendredi */

BEGIN;

INSERT INTO transportscolaire.planning_tour (uuid , "start" , "end" ,
    timestamp_create , timestamp_update , companion_id , created_by_id);
SELECT
    uuid_generate_v4 () AS uuid
    , tourdate + "start" AS start
    , tourdate + "end" AS "END"
    , now()
    , now()
    , CASE WHEN extract(dow FROM tourdate) IN (2 , 5) THEN
        auth_user.id
    END AS companion_id
    , 1
FROM (
    VALUES ('08:10:00'::TIME
            , '09:10:00'::TIME)
        , ('16:10:00'::TIME
            , '17:10:00'::TIME)) AS t ("start"
        , "end")
    CROSS JOIN generate_series('2024-09-02'::DATE , '2025-07-05'::DATE ,
	'1D') AS dates (tourdate)
    , auth_user
WHERE
    -- christelle pour les mardi et vendredi
    username LIKE 'christelle'
    -- Uniquement les lundi, mardi, jeudi et vendredi
        AND (extract(dow FROM tourdate) IN (1 , 2 , 4 , 5)
            -- Hors vacances
            -- Toussaint
            AND tourdate NOT BETWEEN '2024-10-19'::DATE AND '2024-11-03'::DATE
            -- Noel
            AND tourdate NOT BETWEEN '2024-12-21'::DATE AND '2024-01-05'::DATE
            -- Hiver
            AND tourdate NOT BETWEEN '2025-02-22'::DATE AND '2025-03-09'::DATE
            -- Pâques
            AND tourdate NOT BETWEEN '2025-04-19'::DATE AND '2024-05-04'::DATE);

COMMIT;


/* Inscription des tournées d'un accompagnateur (ici Jé) qui 
 * peut faire tous les jeudi des semaines impaires */
BEGIN;

UPDATE
    transportscolaire.planning_tour
SET
    companion_id = 5
WHERE
    companion_id IS NULL
    AND (extract(WEEK FROM START)::INT % 2) <> 0
    AND extract(DOW FROM START) = 4
ON CONFLICT
    DO NOTHING;

COMMIT;


/* Inscription des enfants à toutes les tournées */
BEGIN;

INSERT INTO transportscolaire.planning_tour_kids (tour_id , kid_id)
SELECT
    planning_tour.id
    , planning_kid.id
FROM
    transportscolaire.planning_tour
    CROSS JOIN transportscolaire.planning_kid
WHERE
    name IN ('enfant1' , 'enfant2')
    AND planning_tour.start >= (now())::DATE
ON CONFLICT
    DO NOTHING;

COMMIT;


/* Inscription des enfants toutes les semaines paires 
 * sauf le lundi matin et toutes les semaines impaires 
 * le lundi matin uniquement */
BEGIN;

INSERT INTO transportscolaire.planning_tour_kids (tour_id , kid_id)
SELECT
    planning_tour.id
    , planning_kid.id
FROM
    transportscolaire.planning_tour
    CROSS JOIN transportscolaire.planning_kid
WHERE
    name IN ('enfant1' , 'enfant2')
    AND planning_tour.start > '2024-09-05'
    -- Semaines paires, tous les jours sauf le lundi matin
    AND (((extract(WEEK FROM START)::INT % 2) = 0
            AND NOT ((extract(dow FROM START)::INT = 1)
                AND (extract(hour FROM START)::INT < 12)))
        -- semaines impaires, tous les lundi matin
        OR (((extract(WEEK FROM START)::INT % 2) <> 0)
            AND ((extract(dow FROM START)::INT = 1)
                AND (extract(hour FROM START)::INT < 12))))
ON CONFLICT
    DO NOTHING;

COMMIT;


/* Inscription des enfants tous les jours sauf les mardi et vendredi aprem */
BEGIN;

INSERT INTO transportscolaire.planning_tour_kids (tour_id , kid_id)
SELECT
    planning_tour.id
    , planning_kid.id
FROM
    transportscolaire.planning_tour
    CROSS JOIN transportscolaire.planning_kid
WHERE
    AND name IN ('enfant1' , 'enfant2')
    AND planning_tour.start > '2024-09-05'
    AND NOT (((extract(dow FROM START)::INT = 2)
            AND (extract(hour FROM START)::INT > 12))
        OR (((extract(dow FROM START)::INT = 5)
                AND (extract(hour FROM START)::INT > 12))))
ON CONFLICT
    DO NOTHING;

ROLLBACK;

COMMIT;


/* vue de vérification */
SELECT
    pt.id
    , pt.start::DATE
    , array_agg(pk.name)
    , CASE WHEN (extract(WEEK FROM START)::INT % 2) <> 0 THEN
        'impair'
    ELSE
        'pair'
    END AS pair
    , extract(dow FROM START)
    , CASE WHEN extract(hour FROM START) < 12 THEN
        'morning'
    ELSE
        'afternoon'
    END AS daytime
FROM
    transportscolaire.planning_tour_kids ptk
    JOIN transportscolaire.planning_tour pt ON pt.id = ptk.tour_id
    JOIN transportscolaire.planning_kid pk ON pk.id = ptk.kid_id
WHERE
    pt.start >= now()
    -- AND name IN ('enfant1' , 'enfant2')
GROUP BY
    1
    , 2
    , 4
    , 5
    , 6
ORDER BY
    START::DATE;
