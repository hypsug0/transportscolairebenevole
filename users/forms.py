from django.contrib.auth import get_user_model
from django.forms import ModelForm

User = get_user_model()


class DivRenderer:
    """
    as_div: abstract class
    """

    def as_div(self):
        "Return this form rendered as HTML <divs>s - according to client-Layout."
        return self._html_output(
            normal_row='<div class="field" %(html_class_attr)s> %(errors)s%(field)s%(help_text)s %(label)s</div>',
            error_row="<div%s</div>",
            row_ender="</div> dfdf",
            help_text_html='<br><span class="help">%s</span>',
            errors_on_separate_row=False,
        )

    pass


class UserForm(ModelForm, DivRenderer):
    class Meta:
        model = User
        fields = ["username", "email", "first_name", "last_name"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs["class"] = "input"
