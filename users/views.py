from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import UpdateView

from .forms import UserForm

User = get_user_model()


class AuthUserDetail(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = User
    form_class = UserForm
    template_name = "user_detail.html"
    success_url = reverse_lazy("my-profile")
    success_message = _("%(username)s profile was updated successfully")

    def get_object(self):
        return get_object_or_404(self.model, pk=self.request.user.pk)
