from django.urls import path

from .views import AuthUserDetail

urlpatterns = [
    path("accounts/profile/", AuthUserDetail.as_view(), name="my-profile")
]
